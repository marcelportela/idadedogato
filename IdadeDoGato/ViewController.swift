//
//  ViewController.swift
//  IdadeDoGato
//
//  Created by Marcel Portela on 15/12/15.
//  Copyright © 2015 ISPM. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var catAgeEntry: UITextField!
    @IBOutlet weak var catAgeResult: UILabel!
    
    @IBAction func discoverCatAge(sender: AnyObject) {
        var catAge = Int(catAgeEntry.text!)!
        catAge = catAge * 7
        catAgeResult.text = "A idade do seu gato é \(catAge)"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

